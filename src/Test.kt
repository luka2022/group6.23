fun main() {
}
class First constructor(var x:Int, var y:Int) {
    var xx = 0
    var yy = 0

    init {
       xx = this.x
       yy = this.y
    }

    override fun toString(): String {
        return "$x, $y"
    }

    fun equal(n1: First, n2: First): Boolean {
        return (n1.x == n2.x && n1.y == n2.y)
    }
    fun moveto(a: Int, b: Int): String {
        x += a
        y += b
        xx += a
        yy += b
        return "\n X: $x, Y: $y"
    }}
class Fraction {
    var numerator: Double = 0.0
    var denominator: Double = 0.0

    fun printFraction() {
        println("$this.numerator} / ${this.denominator}")
    }

    override fun toString(): String {
        return "$numerator / $denominator"
    }

    fun Subtraction(dennum:Fraction): Any {
        println(dennum.numerator - dennum.denominator)
        return dennum.numerator - dennum.denominator
    }

    fun Division(dennum: Fraction): Any {
        println(dennum.numerator / dennum.denominator)
        return dennum.numerator / dennum.denominator
    }}
